import { Course } from "./classes/Course";
import { Professor } from "./classes/Professor";
import { Student } from "./classes/Student";
import University from "./classes/University";
import { Degree, Faculty } from "./types/enum";


// Professor

const Professor1 = new Professor("1", "Andreev Andrey", 40, "male");
const Professor2 = new Professor("1", "Andreev Andrey", 40, "male");

console.log(Professor1.calculateSalary());


//University

const ZNU = new University("1", "ZNU", "Zaporizhzhya", 4);

ZNU.addFaculty({id: "1", name: Faculty.Education});

ZNU.addProfessor(Professor1);

console.log(ZNU.getInfo());

//Student

const Student1 = new Student("1", "Romanov Roman", 20, "male", Degree.Bachelor, 1, []);

//Course

const IMT = new Course("1", "IMT", Professor1, 5, false);

IMT.addStudent(Student1);
IMT.courseSrudents;
console.log(IMT.courseProfessor);

//Professor to Student 

Professor2.rateStudent(Student1, IMT, 4);

console.log(Student1.getInfo())