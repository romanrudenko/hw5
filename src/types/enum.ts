export enum Role {
  AssociateProfessor = "ASSOCIATE_PROFESSOR",
  Proffesor = "PROFESSOR",
  SeniorReseacher = "SENIOR_RESEACHER"

}

export enum Faculty {
  Math = "MATH",
  Economics = "ECONOMICS",
  Art = "ART",
  Education = "EDUCATION",
  Engineering = "ENGINEERING",
  Law = "LAW",
  InformationTechnology = "INFORMATION_TECHNOLOGY"
}

export enum Degree {
  Bachelor = "BACHELOR",
  Master = "MASTER",
  DoctorOfPhilosophy = "DOCTOR_OF_PHILOSOPHY"
}