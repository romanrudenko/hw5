import { Person, IFaculty, Mark } from "../interfaces/interfaces";
import { Degree } from "./enum";
import { AssociateProfessor, Professor, SeniorReseacher } from "../classes/Professor";
import { Student } from "../classes/Student";

export type UniversityWorkers =
  | AssociateProfessor
  | Professor
  | SeniorReseacher;

export type StudentType = Person & {
  degree: Degree;
  universityYear: number;
  marks: Array<Mark>;
};

export type ReadonlyFaculty = Readonly<IFaculty>;

export function isStudent(student: Person | Student): student is Student {
  return (student as Student).universityYear !== 0;
}