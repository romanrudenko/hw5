import { Student } from "./Student";
import { LogGetterSetter, LogMethod, LogProperty} from "../decorators/decorators";
import { ICourse, SpecialCourse } from "../interfaces/interfaces";
import { UniversityWorkers } from "../types/types";

export class Course implements ICourse, SpecialCourse {
  @LogProperty
  public _students: Student[] = [];

  constructor(
    public id: string,
    public name: string,
    public professor: UniversityWorkers,
    public credits: number,
    public special: boolean
  ) {}

  @LogGetterSetter
  get courseProfessor() {
    return this.professor;
  }

  set courseProfessor(value: UniversityWorkers) {
    this.professor = value;
  }

  @LogGetterSetter
  get courseSrudents() {
    return this._students;
  }

  @LogMethod
  addStudent(student: Student): void {
    this._students.push(student);
  }

  @LogMethod
  deleteStudent(studentId: string): void {
    this._students = this._students.filter(
      (student) => studentId !== student.id
    );
  }
}
