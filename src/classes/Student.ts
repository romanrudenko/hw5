import { Degree } from "../types/enum";
import { Mark } from "../interfaces/interfaces";
import { StudentType } from "../types/types";

export class Student implements StudentType {
  constructor (
    public id: string,
    public name: string,
    public age: number,
    public gender: string,
    public degree: Degree,
    public universityYear: number,
    public marks: Array<Mark>
  ) {}

  getInfo() {
    return {
      id: this.id,
      name: this.name,
      age: this.age,
      gender: this.gender,
      degree: this.degree,
      universityYear: this.universityYear,
      marks: this.marks
    }
  }
}