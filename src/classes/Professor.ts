import { Course } from "./Course";
import { Student } from "./Student";
import { Role } from "../types/enum";

abstract class ProfessorBase {
  private _salary: number;
  protected _name: string;

  constructor(
    public id: string,
    name: string,
    public age: number,
    public gender: string,
    public role: Role
  ) {
    this._salary = 0;
    this._name = name;
  }

  get salary(): number {
    return this._salary;
  }

  set salary(value: number) {
    this._salary = value;
  }

  rateStudent(student: Student, course: Course, mark: number): void {
    if (!course._students.includes(student)) {
      throw new Error("Student don't belong to this course");
    }
    student.marks.push({ courseName: course.name, mark });
  }

  abstract calculateSalary(): number;
}

export class AssociateProfessor extends ProfessorBase {
  constructor(id: string, name: string, age: number, gender: string) {
    super(id, name, age, gender, Role.AssociateProfessor);
  }

  calculateSalary(): number {
    return 120000;
  }
}

export class Professor extends ProfessorBase {
  constructor(id: string, name: string, age: number, gender: string) {
    super(id, name, age, gender, Role.Proffesor);
  }

  calculateSalary(): number {
    return 160000;
  }
}

export class SeniorReseacher extends ProfessorBase {
  constructor(id: string, name: string, age: number, gender: string) {
    super(id, name, age, gender, Role.SeniorReseacher);
  }

  calculateSalary(): number {
    return 20000;
  }
}
