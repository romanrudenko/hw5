import { IFaculty, IUniversity } from "../interfaces/interfaces";
import { UniversityWorkers } from "../types/types";

export default class University implements IUniversity {
  private _professors: UniversityWorkers[] = [];
  private _faculties: IFaculty[] =[];

  constructor(
    public id: string,
    public name: string,
    public city: string,
    public accredetationLevel: number,
  ) {}

  get faculties() {
    return this._faculties;
  }

  get professors() {
    return this._professors;
  }

  addFaculty(faculty: IFaculty): void {
    this._faculties.push(faculty);
  }

  addProfessor(professor: UniversityWorkers): void {
    this._professors.push(professor);
  }

  deleteFaculty(facultyId: string): void {
    this._faculties = this._faculties.filter((faculty) => faculty.id !== facultyId);
  }

  deleteProfessor(professorId: string): void {
    this._professors = this._professors.filter((professor) => professor.id !== professorId);
  }

  getInfo() {
    return {
      name: this.name,
      city: this.city,
      accredetationLevel: this.accredetationLevel,
    };
  }
}
