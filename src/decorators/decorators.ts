export function LogGetterSetter(
  target: any,
  propertyKey: string | symbol,
  descriptor: PropertyDescriptor
) {
  const getter = descriptor.get;
  const setter = descriptor.set;
  const key = String(propertyKey);

  if (getter) {
    descriptor.get = function () {
      console.log(`Getting ${key}`);
      return getter.call(this);
    };
  }

  if (setter) {
    descriptor.set = function (value: any) {
      console.log(`Setting ${key} to ${value}`);
      setter.call(this, value);
    };
  }
}

export function LogMethod(
  target: any,
  key: string | symbol,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;
  descriptor.value = function (...args: any[]): any {
    console.log("Calling method with arguments: ", JSON.stringify(args));
    return originalMethod.apply(this, args);
  };
  return descriptor;
}

export function LogProperty(target: any, propertyKey: string) {
  let value = target[propertyKey];

  const getter = function () {
    console.log(`Getting property ${propertyKey}`);
    return value;
  };

  const setter = function (newValue: any) {
    console.log(`Setting property ${propertyKey} to ${JSON.stringify(newValue)}`);
    value = newValue;
  };

  Object.defineProperty(target, propertyKey, {
    get: getter,
    set: setter,
    enumerable: true,
    configurable: true,
  });
}
