import { Faculty } from "../types/enum";
import { UniversityWorkers } from "../types/types";

export interface IUniversity {
  name: string;
  city: string;
  accredetationLevel: number;
}

export interface Person {
  id: string;
  name: string;
  age: number;
  gender: string;
}

export interface IFaculty {
  id: string;
  name: Faculty;
}

export interface ICourse {
  id: string;
  name: string;
  professor: UniversityWorkers;
  credits: number;
}

export interface SpecialCourse extends ICourse {
  special: boolean
}

export interface Mark {
  courseName: string;
  mark: number;
}
